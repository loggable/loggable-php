<?php 

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use Loggable\Loggable;


Loggable::setAPIKey('xxxxxxxx');

echo "<pre>";
print_r(Loggable::log(Loggable::success, "Test Message"));
echo "</pre>";
