<?php


namespace Loggable;

//define(LoggableAPIKey, 'qwertyuiop');

class Loggable {
    
    const error = 'error';
    const warning = 'warning';
    const success = 'success';
    const info = 'info';
    
    protected static $LoggingURL = "http://api.loggable.net/v1/log.json";
    
    public static $APIKey = LoggableAPIKey;
    
    public function setAPIKey($APIKey){
        self::$APIKey = $APIKey;
    }
    
    public function log($type, $message){
        
        
        $data = array(
            'apiKey' => self::$APIKey,
            'type'  => $type,
            'message'   =>$message
        );
        
        
        $reply = self::sendLogAction($data);
        
       
        return json_decode($reply);
    }
    
    private function sendLog($data){
        
        //cant we fork? if so lets do it!!
        if (function_exists('pcntl_fork')){
            $pid = pcntl_fork();
            
            //what ever the outcome we need to sent the log... the if may not be needed?
            if ($pid == -1) {
                return self::sendLogAction($data);
                 die('could not fork');
            } else if ($pid) {
                 // we are the parent
                 //pcntl_wait($status); //Protect against Zombie children
                 return self::sendLogAction($data);
                 die('we are the parent');
            } else {
                 // we are the child
                 return self::sendLogAction($data);
                 
            }
        }else{
             //no forking lets jus sent the log   
             return self::sendLogAction($data);
        }
    } 
    
    private function sendLogAction($data){
        
        
        if (ini_get('allow_url_fopen') == 1 && function_exists('file_get_contents')){
            return file_get_contents(self::$LoggingURL .'?' . http_build_query($data));
        }
    }
    
}

?>